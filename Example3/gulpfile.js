'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');

sass.compiler = require('node-sass');

gulp.task('sass', function () {
    return gulp.src('./sass/*.scss')
        .pipe(sass(
            {
                outputStyle: 'expanded'
            }
            ).on('error', sass.logError))
        .pipe(autoprefixer(
            {
                overrideBrowserslist:  ['last 2 versions'],
                cascade: false
            }
            ))
        .pipe(gulp.dest('./css'));
});

gulp.task('sass:watch', function () {
    gulp.watch('./sass/*.scss', gulp.series('sass'));
});