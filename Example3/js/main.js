'use strict';

var buttonOpenModal = document.getElementById('btn__open__modal');
var buttonCloseModal = document.getElementById('btn__close__modal');
var modalWindow = document.getElementById('modal__window');
var fon = document.getElementById('fon');
var buttonOpenModalEnter = document.getElementById('btn__open__modal--enter');
var buttonCloseModalEnter = document.getElementById('btn__close__modal--enter');
var modalWindowEnter = document.getElementById('modal__window--enter');
var fonEnter = document.getElementById('fon--enter');

buttonOpenModal.addEventListener('click', callBackModalWindow);
buttonCloseModal.addEventListener('click', callBackModalWindow);
fon.addEventListener('click', callBackModalWindow);
buttonOpenModalEnter.addEventListener('click', callBackModalWindowEnter);
buttonCloseModalEnter.addEventListener('click', callBackModalWindowEnter);
fonEnter.addEventListener('click', callBackModalWindowEnter);


function callBackModalWindow(event) {
    event.preventDefault();
    modalWindow.classList.toggle('modal__window--show');
    fon.classList.toggle('fon--show');
}

function callBackModalWindowEnter(event) {
    event.preventDefault();
    modalWindowEnter.classList.toggle('modal__window--show');
    fonEnter.classList.toggle('fon--show');
}
