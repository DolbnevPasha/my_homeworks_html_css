'use strict';

/*  Filter block  */

let filter = document.getElementById('filter');
let filterInputs = filter.getElementsByTagName('input');
let products = document.getElementsByClassName('product');

for (let i = 0; i < filterInputs.length; i++) {
    filterInputs[i].addEventListener('click',showCallBack);
}

function showCallBack(event){

    for (let i = 0; i < products.length; i++) {
        if(products[i].classList.contains('show')){
            products[i].classList.remove('show');
        }
    }

    let isOnFilters = false;

    for (let i = 0; i < filterInputs.length; i++) {


        if(filterInputs[i].checked){

            isOnFilters = true;
            
            for (let j = 0; j < products.length; j++) {

                if(products[j].classList.contains(filterInputs[i].name)){
                    products[j].classList.add('show');
                }
            }
        }
    }

    if(!isOnFilters){
        for (let i = 0; i < products.length; i++) {
            products[i].classList.add('show');
        }
    }
}

/* Popup block */

let buttonsOpenModal = document.getElementsByClassName('btn__open__modal');        
let buttonCloseModal = document.getElementById('btn__close__modal');      
let modalWindow = document.getElementById('modal__window');               
let fon = document.getElementById('fon');                                 

for (let i = 0; i < buttonsOpenModal.length; i++) {
    buttonsOpenModal[i].addEventListener('click', callBackModalWindow);
    buttonsOpenModal[i].addEventListener('click', callBackContentModalWindow);
}               
buttonCloseModal.addEventListener('click', callBackModalWindow);              
fon.addEventListener('click', callBackModalWindow);                          

function callBackModalWindow(event) {                                               
    event.preventDefault();
    modalWindow.classList.toggle('modal__window--show');                     
    fon.classList.toggle('fon--show');                                    
}

let title = document.getElementById('title');
let titleOption1 = document.getElementById('title__option-1');
let contentOption1 = document.getElementById('content__option-1');
let contentOption2 = document.getElementById('content__option-2');
let image = document.getElementById('image');
let price = document.getElementById('price');

function callBackContentModalWindow(event) {
    event.preventDefault();
    let cardProduct = this.parentElement;

    if(cardProduct.classList.contains('smartphone')){
        let good = goods.find(item => item.category === 'smartphone');
        title.innerHTML = good.name;
        image.src = good.img;
        titleOption1.innerHTML = "Память";
        price.innerHTML = '$' + good.price + '<sup>.00</sup>'
        contentOption1.innerHTML = '';
        good.memory.forEach(item => {
            contentOption1.innerHTML += '<option value='+item+'>'+item+'Gb</option>';
        });
        contentOption2.innerHTML = '';
        good.colors.forEach(item => {
            contentOption2.innerHTML += '<option value='+item+'>'+item+'</option>';
        });
    }

    if(cardProduct.classList.contains('tablet')){
        let good = goods.find(item => item.category === 'tablet');
        title.innerHTML = good.name;
        image.src = good.img;
        titleOption1.innerHTML = "Память";
        price.innerHTML = '$' + good.price + '<sup>.00</sup>'
        contentOption1.innerHTML = '';
        good.memory.forEach(item => {
            contentOption1.innerHTML += '<option value='+item+'>'+item+'Gb</option>';
        });
        contentOption2.innerHTML = '';
        good.colors.forEach(item => {
            contentOption2.innerHTML += '<option value='+item+'>'+item+'</option>';
        });
    }

    if(cardProduct.classList.contains('watch')){
        let good = goods.find(item => item.category === 'watch');
        title.innerHTML = good.name;
        image.src = good.img;
        titleOption1.innerHTML = "Размер";
        price.innerHTML = '$' + good.price + '<sup>.00</sup>'
        contentOption1.innerHTML = '';
        good.size.forEach(item => {
            contentOption1.innerHTML += '<option value='+item+'>'+item+'См</option>';
        });
        contentOption2.innerHTML = '';
        good.colors.forEach(item => {
            contentOption2.innerHTML += '<option value='+item+'>'+item+'</option>';
        });
    }

    if(cardProduct.classList.contains('accessories')){
        let good = goods.find(item => item.category === 'accessories');
        title.innerHTML = good.name;
        image.src = good.img;
        titleOption1.innerHTML = "Заряд";
        price.innerHTML = '$' + good.price + '<sup>.00</sup>'
        contentOption1.innerHTML = '';
        good.charge.forEach(item => {
            contentOption1.innerHTML += '<option value='+item+'>'+item+'Ч</option>';
        });
        contentOption2.innerHTML = '';
        good.colors.forEach(item => {
            contentOption2.innerHTML += '<option value='+item+'>'+item+'</option>';
        });
    }

    if(cardProduct.classList.contains('other')){
        let good = goods.find(item => item.category === 'other');
        title.innerHTML = good.name;
        image.src = good.img;
        titleOption1.innerHTML = "Мощность";
        price.innerHTML = '$' + good.price + '<sup>.00</sup>'
        contentOption1.innerHTML = '';
        good.power.forEach(item => {
            contentOption1.innerHTML += '<option value='+item+'>'+item+'А</option>';
        });
        contentOption2.innerHTML = '';
        good.colors.forEach(item => {
            contentOption2.innerHTML += '<option value='+item+'>'+item+'</option>';
        });
    }
}

let goods = [
    {
        category: "smartphone",
        name: "IPhone 5",
        img: "images/modal-img.png",
        colors: ['Белый', 'Черный', 'Серебристый', 'Золотой', 'Розовый', ],
        price: 519,
        memory: ['32', '64', '128', '256', '512']
    },
    {
        category: "tablet",
        name: "IPad",
        img: "images/modal-img.png",
        colors: ['Белый', 'Черный', 'Серебристый', 'Золотой', 'Розовый', ],
        price: 840,
        memory: ['32', '64', '128', '256', '512'],
    },
    {
        category: "watch",
        name: "Apple Watch",
        img: "images/modal-img.png",
        colors: ['Белый', 'Черный', 'Серебристый', 'Золотой', 'Розовый', 'Красный'],
        price: 780,
        size: ['36', '40', '44', '48'],
    },
    {
        category: "accessories",
        name: "Airpods",
        img: "images/modal-img.png",
        colors: ['Белый','Серебристый', 'Золотой', 'Розовый'],
        price: 200,
        charge: ['4', '6', '8', '10'],
    },
    {
        category: "other",
        name: "Charger",
        img: "images/modal-img.png",
        colors: ['Белый'],
        price: 30,
        power: ['1', '1.2', '1.4'],
    }
];



/* select2 */
$(document).ready(function() {
    $('.js-example-basic-single').select2({
        containerCssClass: 'my-select',
        dropdownCssClass: 'my-dropdown'
    });
});